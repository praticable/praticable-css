# Praticable.fr styles

All praticable.fr default css styles.

## Installation

### Download

Download and copy this repository to `assets/css`.

### Git submodule

```
git submodule add git@framagit.org:praticable/praticable-css.git assets/css
```

## Custom styles

Add all your custom styles to `src/custom.css`.

## Build

For coding commodity, css files are splitted roughly following the [CUBE CSS method](https://cube.fyi/) in `src/`. Splitted files are imported in `style.css`. CSS imports are supported by browsers but slow since all imported files needs to be downloaded one after another.

Therefore, you should at least concatenate src files, or even build them. Use whatever build tool you want (Gulp, Webpack, Rollup…). Don't want to use complex bundlers or build tools ? Simply concatenate using shell cat command. In a unix terminal, run :

```
cat src/*css > style.bundle.css
```

This simple line concatenates (`cat`) all css files located in src directory(`src/*.css`) to (`>`) `style.bundle.css`. Feel free to custom the file name and to move it where you want (eg. `/dist/`).
